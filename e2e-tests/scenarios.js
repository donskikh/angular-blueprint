'use strict';

describe('Portfolio', function() {

    it('should automatically redirect to /home when location hash is empty', function() {
        browser.get('index.html');
        expect(browser.getLocationAbsUrl()).toMatch("/home");
    });

    it('should have a correct title', function() {
        browser.get('index.html');

        expect(browser.getTitle()).toEqual('Angular Blueprint');
    });

    describe('Home', function() {

        beforeEach(function() {
            browser.get('index.html#/home');
        });

        it('should render home page when user navigates to /home', function() {
            expect(element.all(by.css('.home-root')).first().getText())
                .toMatch(/Angular Blueprint/);
        });
    });

    describe('About', function() {

        beforeEach(function() {
            browser.get('index.html#/about');
        });

        it('should render about page when user navigates to /about', function() {
            expect(element.all(by.css('.about-root')).first().getText())
                .toMatch(/About/);
        });
    });

    describe('Portfoilo', function() {

        beforeEach(function() {
            browser.get('index.html#/portfolio');
        });

        it('should render portfolio page when user navigates to /portfolio', function() {
            expect(element.all(by.css('.portfolio-root')).first().getText())
                .toMatch(/Portfoilo/);
        });
    });

    describe('Contact', function() {

        beforeEach(function() {
            browser.get('index.html#/contact');
        });

        it('should render contact page when user navigates to /contact', function() {
            expect(element.all(by.css('.contact-root')).first().getText())
                .toMatch(/Contact/);
        });
    });
});