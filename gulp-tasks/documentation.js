const gulp = require('gulp'),
    shell = require('gulp-shell'),
    paths = require('./paths.json');

gulp.task('documentation', shell.task([
    'rm -rf  html-documentation',
    './node_modules/.bin/jsdoc src/scripts -r -d html-documentation -R README.md -P package.json'
]));