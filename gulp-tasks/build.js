const gulp = require('gulp'),
    runSequence = require('run-sequence'),
    argv = require('yargs').argv;

require('gulp-stats')(gulp);

gulp.task('build', () => {

    if (argv.production) {
        runSequence('clean', 'scripts', 'styles', 'images', 'fonts', 'html', 'test', 'documentation');
    } else if (argv.skiptests) {
        runSequence('scripts', 'styles', 'images', 'fonts', 'html');
    } else {
        runSequence('scripts', 'styles', 'images', 'fonts', 'html', 'test');
    }
});