const gulp = require('gulp'),
    postcss = require('gulp-postcss'),
    atImport = require('postcss-import'),
    autoprefixer = require('autoprefixer'),
    cssnano = require('cssnano'),
    paths = require('./paths.json'),
    connect = require('gulp-connect'),
    argv = require('yargs').argv;

gulp.task('styles', () => {
    var plugins = [
        atImport,
        autoprefixer({
            browsers: ['last 2 versions']
        })
    ];

    if (argv.production) {
        plugins.push(cssnano);
    }

    return gulp.src(paths.src.styles)
        .pipe(postcss(plugins))
        .pipe(gulp.dest(paths.build.styles))
        .pipe(connect.reload());
});