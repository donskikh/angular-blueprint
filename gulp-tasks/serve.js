const gulp = require('gulp'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('serve', ['build'], () => {
  connect.server({
    root: paths.build.target,
    livereload: true
  });
});