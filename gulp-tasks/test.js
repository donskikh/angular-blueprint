const gulp = require('gulp'),
    Server = require('karma').Server,
    path = require('path'),
    argv = require('yargs').argv;

gulp.task('test', (done) => {
    const server = new Server({
        configFile: path.resolve('karma.conf.js'),
        singleRun: argv.debug ? false : true,
        browsers: argv.debug ? ['Chrome'] : ['PhantomJS']
    }, done);
    
    server.start();
    server.on('browser_error', function (err) {
        throw err;
    });

    return server;
});