const gulp = require('gulp'),
    paths = require('./paths.json');

gulp.task('watch', ['serve'], () => {
    gulp.watch(paths.src.allStyles, ['styles']);
    gulp.watch(paths.src.allScripts, ['browserify-app']);
    gulp.watch(paths.src.data, ['data']);
    gulp.watch(paths.src.html, ['html']);
    gulp.watch(paths.src.allHtml, ['browserify-app']);
});