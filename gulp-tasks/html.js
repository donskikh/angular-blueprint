const gulp = require('gulp'),
    htmlmin = require('gulp-htmlmin'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    revision = require('gulp-rev-append'),
    paths = require('./paths.json');

gulp.task('html', () => {
    return gulp.src(paths.src.html)
        .pipe(gulpif(argv.production, revision()))
        .pipe(gulpif(argv.production, htmlmin({
            collapseWhitespace: true
        })))
        .pipe(gulp.dest(paths.build.target))
        .pipe(connect.reload());
});