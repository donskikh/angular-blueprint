const gulp = require('gulp'),
    eslint = require('gulp-eslint'),
    paths = require('./paths.json');

gulp.task('eslint', () => {
    return gulp.src(paths.src.scripts)
        .pipe(eslint())
        .pipe(eslint.format());
});