const gulp = require('gulp'),
    imageMin = require('gulp-imagemin'),
    jpegtran = require('imagemin-jpegtran'),
    imageminPngquant = require('imagemin-pngquant'),
    newer = require('gulp-newer'),
    paths = require('./paths.json');

const imageminPlugins = [jpegtran(), imageminPngquant()];

gulp.task('images', () => {
    return gulp.src(paths.src.images)
        .pipe(newer(paths.build.images))
        .pipe(imageMin({
            progressive: true,
            use: imageminPlugins
        }))
        .pipe(gulp.dest(paths.build.images));
});