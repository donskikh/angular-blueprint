const gulp = require('gulp'),
    newer = require('gulp-newer'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    streamify = require('gulp-streamify'),
    uglify = require('gulp-uglify'),
    html2Js = require('browserify-ng-html2js'),
    argv = require('yargs').argv,
    gulpif = require('gulp-if'),
    connect = require('gulp-connect'),
    paths = require('./paths.json');

gulp.task('scripts', ['eslint', 'browserify-vendor', 'browserify-app', 'data']);

gulp.task('browserify-vendor', () => {
    return browserify(paths.src.vendorScript)
        .bundle()
        .pipe(source('vendor.js'))
        .pipe(streamify(uglify()))
        .pipe(gulp.dest(paths.build.scripts));
});

gulp.task('browserify-app', () => {
    return browserify(paths.src.appScript, {
            debug: argv.production ? false : true
        })
        .transform(html2Js({
            module: 'templates'
        }))
        .bundle()
        .pipe(source('app.js'))
        .pipe(gulpif(argv.production, streamify(uglify())))
        .pipe(gulp.dest((paths.build.scripts)))
        .pipe(connect.reload());
});

gulp.task('data', () => {
    return gulp.src(paths.src.data)
        .pipe(gulp.dest(paths.build.data))
        .pipe(connect.reload());
});