const gulp = require('gulp'),
    paths = require('./paths.json');

gulp.task('fonts', () => {
    return gulp.src(paths.src.fonts)
        .pipe(gulp.dest(paths.build.fonts));
});