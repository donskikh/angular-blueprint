# Angular Blueprint

Angular Portfolio was built with [Angular 1.5.7](https://angularjs.org) and [Browserify](http://browserify.org).

## Links
[Demo](http://angular-blueprint.donskikh.com/#/home)   
[Source Code](https://bitbucket.org/donskikh/angular-blueprint)   
[Unit tests](http://angular-blueprint.donskikh.com/unit-tests)   
[Documentation](http://angular-blueprint.donskikh.com/documentation)   
[Portfolio](http://www.donskikh.com)

## Prerequisites
* [NodeJs](https://nodejs.org/en/) 4+
* [Gulp](http://gulpjs.com)
* [Protractor](http://www.protractortest.org/#/)


## Run
Just run two commands in shell:   
```npm install```   
and   
```gulp serve```   
It will run local server accessible at
http://localhost:8080 with live-reload ability.

## Get production-ready version
```gulp build --production```   
The result will be placed in the ```./build``` directory.


## Develop
All source code available at the ```./src``` directory.

### Unit tests
Are written with [Jasmine](http://jasmine.github.io) and work with [Karma](https://karma-runner.github.io/0.13/index.html) test-runner.   
Test files placed inside modules.

You can easily run it with:   
```gulp test```   

### E2E tests
Written with [Protractor](http://www.protractortest.org/#/).   
Directory: ```e2e-tests```   

To run please follow the next stepts:   
1) Run app with: ```gulp serve```   
2) Update webdriver-manager ```webdriver-manager update```   
3) Start webdriver-manager ```webdriver-manager start```   
4) Run tests: ```protractor e2e-tests/protractor.conf.js --verbose```

### Documentation
Documentation in html will be available after   
```gulp build --production``` or   
```gulp documentation``` command in the   
```./html-documentation``` directory.

### Gulp
* directory: ```./gulp-tasks```

The next gulp tasks are available in the application:

```gulp build``` (prepare for develop | production; run tests; create documentation. Output: ./build)   
```gulp clean``` (remove the ./build directory)   
```gulp default``` (run build)   
```gulp eslint``` (check the code for style-guide compatibility)   
```gulp images``` (optimize images and copy to the ./build directory)   
```gulp fonts``` (grab and copy fonts to the ./build directory)   
```gulp html``` (minify html, add revision to the resources and move to the ./build directory)   
```gulp scripts``` (browserify assets and copy to the ./build directory)   
```gulp serve``` (run local server with live-reload capability)   
```gulp styles``` (grab imported files, add vendor-prefixes and copy it to the ./build directory)   
```gulp test``` (run unit test)   
```gulp watch``` (watch on html, css, js files)   
```gulp documentation``` (create documentation for the project: ./html-documentation)   

* Default gulp task: ```gulp``` will run build task.
* Every task could be run with the ```--production``` flag.
* Unit tests could be run with the ```--debug``` flag. It will run them with Chrome instead of PhantomJS, and switch Karma's ```singleRun``` parameter to false to allow you to debug everything within browser's  console.

## Licence
The MIT License (MIT)
Copyright (c) 2016 Donskikh A.V.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.