'use strict';

/**
 * @memberOf App
 * @description Application's constants
 * @type {object}
 */
var Constants = {
    'CURRENT_YEAR': new Date().getFullYear(),
    'EMAIL': '',
    'boundaries': {
        'northeast': {
            //New York, New York, USA
            'latitude': 40.78505507143635,
            'longitude': -73.62762451171875
        },
        'southwest': {
            'latitude': 40.576767882742125,
            'longitude': -74.410400390625
        }
    },
    'marker': {
        'id': 1,
        //New York, New York, USA
        'coordinates': {
            'latitude': 40.72,
            'longitude': -73.997
        }
    }
};

module.exports = Constants;