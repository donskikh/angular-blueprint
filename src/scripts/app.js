'use strict';

var angular = require('angular'),
    config = require('./config'),
    constants = require('./constants'),
    run = require('./run');

require('angular-route');
require('angular-simple-logger');
require('angular-google-maps');

config.$inject = ['$routeProvider', 'uiGmapGoogleMapApiProvider'];
run.$inject = ['$rootScope', 'constants'];


/**
 * Main appplication module
 * @namespace App
 * @requires module:templates
 * @requires module:ngRoute
 * @requires module:uiGmapgoogle-maps
 * @requires module:App.HomeModule
 * @requires module:App.AboutModule
 * @requires module:App.PortfolioModule
 * @requires module:App.ContactModule
 * @requires module:App.NavigationModule
 */
angular.module('App', [
    'templates',
    'ngRoute',
    'uiGmapgoogle-maps',
    'App.Home',
    'App.About',
    'App.Portfolio',
    'App.Contact',
    'App.Navigation'
])
.config(config)
.constant('constants', constants)
.run(run);

require('./modules');