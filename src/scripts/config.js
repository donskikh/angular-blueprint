    'use strict';

require('./modules/home/home.html');
require('./modules/about/about.html');
require('./modules/portfolio/portfolio.html');
require('./modules/contact/contact.html');

/**
 * @class App.Config
 * @memberOf App
 * @constructor
 * @description Application's config
 * @param {*} $routeProvider Angular {@link https://docs.angularjs.org/api/ngRoute/provider/$routeProvider|$routeProvider} service
 * @param {*} uiGmapGoogleMapApiProvider Google {@link https://github.com/angular-ui/angular-google-maps|maps} service
 */
var Config = function($routeProvider, uiGmapGoogleMapApiProvider) {

    $routeProvider
        .when('/home', {
            templateUrl: 'home.html',
            controller: 'HomeCtrl',
            controllerAs: 'homeCtrl'
        })
        .when('/about', {
            templateUrl: 'about.html',
            controller: 'AboutCtrl',
            controllerAs: 'aboutCtrl'
        })
        .when('/portfolio', {
            templateUrl: 'portfolio.html',
            controller: 'PortfolioCtrl',
            controllerAs: 'portfolioCtrl'
        })
        .when('/contact', {
            templateUrl: 'contact.html',
            controller: 'ContactCtrl',
            controllerAs: 'contactCtrl'
        })
        .otherwise({
            redirectTo: '/home'
        });

    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyC0ia5Nlcvr5sDwqH3pxDBov2i3ze_vqP0',
        v: '3.23',
        libraries: 'weather,geometry,visualization'
    });
}

module.exports = Config;