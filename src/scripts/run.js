'use strict';

/**
 * @class App.Run
 * @memberOf App
 * @constructor
 * @description Application's run class
 * @param {*} $rootScope Angular's {@link https://docs.angularjs.org/api/ng/service/$rootScope|https://docs.angularjs.org/api/ng/service/$rootScope}
 * @param {object} constants Application's constants
 */
var Run = function($rootScope, constants) {
    $rootScope.CURRENT_YEAR = constants.CURRENT_YEAR;
    $rootScope.EMAIL = constants.EMAIL;
}

module.exports = Run;