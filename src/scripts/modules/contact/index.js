'use strict';
require('angular');

var ContactController = require('./contact.ctrl');
ContactController.$inject = ['$http', 'constants'];

/**
 * @namespace App.ContactModule
 * @description Contact module
 */
var ContactModule = angular.module('App.Contact', [])
    .controller('ContactCtrl', ContactController);

module.export = ContactModule;