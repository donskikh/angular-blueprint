'use strict';

describe('App.Contact module', function() {

    var scope, ctrl;

    beforeEach(module('App.Contact'));
    
    beforeEach(function() {
        module('App.Contact', function($provide) {
            $provide.constant('constants', {});
        });
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();

        ctrl = $controller('ContactCtrl', {
            $scope: scope
        });
    }));

    it('should have ContactCtrl defined', function() {
        expect(ctrl).toBeDefined();
    });
});