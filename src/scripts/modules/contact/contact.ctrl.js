'use strict';

/**
* @class App.ContactModule.ContactCtrl
* @memberOf App.ContactModule
* @constructor
* @description Contact module controller
* @param {*} $http Angular {@link https://docs.angularjs.org/api/ng/service/$http|$http} service
* @param {object} constants Application's constants
*/
var ContactCtrl = function($http, constants) {
    var that = this;
    that.bounds = constants.boundaries;
    that.marker = constants.marker;
    that.communicationChannels = [];

    activate();

    /**
     * @description Rus when controller is activated
     * @memberOf App.ContactModule.ContactCtrl
     */
    function activate() {
        // Get contacts
        $http.get('data/contacts.json').success(function(data) {
            that.communicationChannels = data;
        });
    }
};

module.exports = ContactCtrl;