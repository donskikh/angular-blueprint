'use strict';
require('angular');

var PortfolioCtrl = require('./portfolio.ctrl');

PortfolioCtrl.$inject = ['$http'];

/**
 * @namespace App.PortfolioModule
 * @description Home module
 */
var PortfolioModule = angular.module('App.Portfolio', [])
    .controller('PortfolioCtrl', PortfolioCtrl);

module.export = PortfolioModule;