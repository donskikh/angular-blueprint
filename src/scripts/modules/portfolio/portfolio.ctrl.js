'use strict';

/**
 * @class App.PortfolioModule.PortfolioCtrl
 * @memberOf App.PortfolioModule
 * @constructor
 * @description Portfolio controller
 * @param {*} $http Angular {@link https://docs.angularjs.org/api/ng/service/$http|$http} service
 */
var PortfolioCtrl = function($http) {
    var that = this;

    active();

    /**
     * @description Rus when controller is activated
     * @memberOf App.PortfolioModule.PortfolioCtrl
     */
    function active() {

    }
};

module.exports = PortfolioCtrl;