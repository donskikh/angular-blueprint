'use strict';

describe('App.Portfolio module', function() {

    var scope, portfolioCtrl, projectDetailsCtrl;

    beforeEach(module('App.Portfolio'));

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();

        portfolioCtrl = $controller('PortfolioCtrl', {
            $scope: scope
        });

        it('should have PortfolioCtrl defined', function() {
            expect(portfolioCtrl).toBeDefined();
        });
    }));
});