describe('directive: navigation', function() {
    var element,
        $compile,
        $rootScope,
        $location,
        isolatedScope;

    beforeEach(module('App.Navigation'));

    beforeEach(module('templates'));

    beforeEach(inject(function(_$compile_, _$rootScope_, _$location_) {
        // The injector unwraps the underscores (_) from around the parameter names when matching
        $compile = _$compile_;
        $rootScope = _$rootScope_;
        $location = _$location_;

        spyOn($location, 'path').and.returnValue('/home');

        element = $compile('<nav navigation></nav>')($rootScope);
        $rootScope.$digest();

        isolatedScope = element.isolateScope();
    }));

    it('should contain 4 links', function() {
        expect(isolatedScope.links.length).toEqual(4);
    });

    it('should contain falsy `collapsed` property', function() {
        expect(isolatedScope.collapsed).toBeTruthy();
    });

    describe('interface', function() {
        it('should contain the next functions', function() {

            expect(isolatedScope.isActive).toBeDefined();
            expect(isolatedScope.toggleNavigation).toBeDefined();
            expect(isolatedScope.closeNavigation).toBeDefined();
        });
    });

    describe('isActive()', function() {
        it('should return true if current path equals passed parameter', function() {
            expect(isolatedScope.isActive('/home')).toBeTruthy();
        });
    });

    describe('toggleNavigation()', function() {

        it('should toggle `collapsed property state`', function() {
            isolatedScope.collapsed = false;
            isolatedScope.toggleNavigation();

            expect(isolatedScope.collapsed).toBeTruthy();
        });
    });

    describe('closeNavigation()', function() {

        it('should set `collapsed` property to `true`', function() {
            isolatedScope.collapsed = false;
            isolatedScope.closeNavigation();

            expect(isolatedScope.collapsed).toBeTruthy();
        });
    });
});