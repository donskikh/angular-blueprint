'use strict';
require('angular');

var NavigationDirective = require('./navigation.directive');
NavigationDirective.$inject = ['$location'];

/**
 * @namespace App.NavigationModule
 * @description Home module
 */
var NavigationModule =  angular.module('App.Navigation', [])
.directive('navigation', NavigationDirective);

module.exports = NavigationModule;