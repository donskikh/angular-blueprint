'use strict';

require('./navigation.html');

/**
 * @class App.NavigationModule.NavigationDirective
 * @memberOf App.NavigationModule
 * @constructor
 * @description Navigation module directive
 * @param {*} $location Angular {@link https://docs.angularjs.org/api/ng/service/$location|$location} service
 */
var NavigationDirective = function($location) {
    return {
        restrict: 'AE',
        replace: true,
        scope: {},
        templateUrl: 'navigation.html',
        link: function($scope) {

            $scope.isActive = isActive;
            $scope.toggleNavigation = toggleNavigation;
            $scope.closeNavigation = closeNavigation;

            $scope.collapsed = true;

            $scope.links = [{
                'target': '/home',
                'name': 'Home'
            }, {
                'target': '/about',
                'name': 'About'
            }, {
                'target': '/portfolio',
                'name': 'Portfolio'
            }, {
                'target': '/contact',
                'name': 'Contact'
            }];

            /**
             * Check is current item active (match with the location)
             * @memberOf App.NavigationModule.NavigationDirective
             * @param   {string} location Current location
             * @returns {boolean}
             */
            function isActive(location) {
                return location === $location.path();
            };

            /**
             * Toggle mobile navigation
             * @memberOf App.NavigationModule.NavigationDirective
             */
            function toggleNavigation() {
                $scope.collapsed = !$scope.collapsed;
            }

            /**
             * Close mobile navigation
             * @memberOf App.NavigationModule.NavigationDirective
             */
            function closeNavigation() {
                $scope.collapsed = true;
            }
        }
    };
};

module.exports = NavigationDirective;