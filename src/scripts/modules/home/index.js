'use strict';
require('angular');

var HomeCtrl = require('./home.ctrl');
HomeCtrl.$inject = ['constants'];

/**
 * @namespace App.HomeModule
 * @description Home module
 */
var HomeModule = angular.module('App.Home', [])
    .controller('HomeCtrl', HomeCtrl);

module.exports = HomeModule;