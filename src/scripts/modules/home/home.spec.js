'use strict';

describe('App.Home module', function() {

    var scope, ctrl;

    beforeEach(module('App.Home'));
    
    beforeEach(function() {
        module('App.Home', function($provide) {
            $provide.constant('constants', {});
        });
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();

        ctrl = $controller('HomeCtrl', {
            $scope: scope
        });
    }));

    it('should have HomeCtrl defined', function() {
        expect(ctrl).toBeDefined();
    });
});