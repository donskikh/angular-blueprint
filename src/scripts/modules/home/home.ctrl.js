'use strict';

/**
* @class App.HomeModule.HomeCtrl
* @memberOf App.HomeModule
* @constructor
* @description Home module controller
* @param {object} constants Application's constants
*/
var HomeCtrl = function(constants) {
    this.email = constants.EMAIL;
};

module.exports = HomeCtrl;