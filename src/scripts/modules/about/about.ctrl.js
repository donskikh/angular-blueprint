'use strict';

/**
 * @class App.AboutModule.AboutCtrl
 * @memberOf App.AboutModule
 * @constructor
 * @description About module controller
 * @param {*} $http Angular {@link https://docs.angularjs.org/api/ng/service/$http|$http} service
 */
var AboutCtrl = function($http) {
    var that = this;

    activate();

    /**
     * @description Runs when controller is activated
     * @memberOf App.AboutModule.AboutCtrl
     */
    function activate() {
    }
};

module.exports = AboutCtrl;