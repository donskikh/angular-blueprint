'use strict';

describe('App.About module', function() {

    var scope, ctrl;

    beforeEach(function() {
        module('App.About', function($provide) {
            $provide.constant('constants', {});
        });
    });

    beforeEach(inject(function($rootScope, $controller) {
        scope = $rootScope.$new();

        ctrl = $controller('AboutCtrl', {
            $scope: scope
        });
    }));

    it('should have AboutCtrl defined', function() {
        expect(ctrl).toBeDefined();
    });
});