'use strict';
require('angular');

var AboutController = require('./about.ctrl');
AboutController.$inject = ['$http'];

/**
 * @namespace App.AboutModule
 * @description About module
 */
var AboutModule = angular.module('App.About', [])
    .controller('AboutCtrl', AboutController);

module.exports = AboutModule;